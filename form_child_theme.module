<?php

/*
 * @file
 * Form Child Theme Module
 *
 * This is a helper module that allows other modules to theme the child elements of radios
 * and checkboxes elements without having to manually expland the children.
 *
 */

/**
 * Implements hook_form_alter().
 *
 * Run child theme expander on all forms.
 */
function form_child_theme_form_alter(&$form, $form_state, $form_id) {
  form_child_theme_add_expander($form);
}

/**
 * Function to navigate through form elements and find radios and checkboxes to process.
 *
 * @param $element
 *   The form element being processed.
 *
 */
function form_child_theme_add_expander(&$element) {
  foreach (element_children($element) as $key) {
    if (!empty($element[$key]['#type'])) {
      switch ($element[$key]['#type']) {
        case 'radios':
        case 'checkboxes':
          if (isset($element[$key]['#options']) && isset($element[$key]['#child_theme'])) {
            foreach ($element[$key]['#options'] as $child_key => $child_value) {
              if (!isset($element[$key][$child_key]['#theme'])) {
                $element[$key][$child_key]['#theme'] = $element[$key]['#child_theme'];
              }
            }
          }
          break;
      }
    }
    // Recursively call function to process any children.
    form_child_theme_add_expander($element[$key]);
  }
}